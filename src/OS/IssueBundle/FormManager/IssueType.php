<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace OS\IssueBundle\FormManager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Description of IssueType
 *
 * @author ouardisoft
 */
class IssueType extends AbstractType
{
    /**
     * 
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $issueType = array('cc' => 'Customer Care', 'dj' => 'Direction Juridique');
        $status = array('instance' => 'Instance', 'courrier' => 'Courrier en validation', 'traite' => 'Triaté');
        $canal = array('courrier' => 'Courrier', 'email' => 'Email', 'present' => 'Présence au siège', 'fax' => 'Fax', 'autre' => 'Autre');

        // Form Method
        $builder->setMethod('POST');

        /**
        $builder->add(
            'issueType',
            'choice',
            array(
                'label' => 'Traité Par',
                'choices' => $issueType,
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );
        */
        $builder->add(
            'projet',
            'hidden',
            array()
        );

        $builder->add(
            'bien',
            'text',
            array(
                'label' => 'Bien',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'client',
            'text',
            array(
                'label' => 'Client',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'canal',
            'choice',
            array(
                'label' => 'Canal',
                'choices' => $canal,
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank()),
            )
        );

        $builder->add(
            'nom',
            'text',
            array(
                'label' => 'Nom',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'sujet',
            'textarea',
            array(
                'label' => 'Sujet',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'motif',
            'text',
            array(
                'label' => 'Motif',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'action',
            'textarea',
            array(
                'label' => 'Action',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'intervenant',
            'text',
            array(
                'label' => 'Intervenant',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'observation',
            'textarea',
            array(
                'label' => 'Observation',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'responseDate',
            'date',
            array(
                'label' => 'Date réponse',
                'widget' => 'single_text',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'isreceive',
            'choice',
            array(
                'label' => 'Réponse envoyé au client',
                'attr' => array('class' => 'span12'),
                'expanded' => true,
                'choices' => array(0 => 'Non', 1 => 'Oui'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'customerResponseDate',
            'date',
            array(
                'label' => 'Date réponse client',
                'widget' => 'single_text',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'feedback',
            'textarea',
            array(
                'label' => 'Commentaire sur la réponse',
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        $builder->add(
            'status',
            'choice',
            array(
                'label' => 'Statut',
                'choices' => $status,
                'attr' => array('class' => 'span12'),
                'constraints' => array(new NotBlank())
            )
        );

        // Form Submit Button
        $builder->add('add', 'submit', array('label' => 'Ajouter Réclamation', 'attr' => array('class' => 'span3 btn btn-primary')));
    }

    /**
     * 
     * @param \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('required' => false));
    }

    /**
     * 
     * @param \Symfony\Component\Form\FormView $view
     * @param \Symfony\Component\Form\FormInterface $form
     * @param array $options
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if (null !== $form->getViewData()->client) {
            $child = $view->children['add'];
            $child->vars['label'] = 'Enregistrer les modifications';
        }

        parent::finishView($view, $form, $options);
    }

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return 'issue';
    }
}

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace OS\IssueBundle\FormManager;

use OS\ToolsBundle\FormManager\AbstractFormHandler;
use Symfony\Component\Form\Form;
use OS\CommonBundle\Entity\Issues;

/**
 * Description of IssueHandler
 *
 * @author ouardisoft
 */
class IssueHandler extends AbstractFormHandler
{

    /**
     * 
     * @param \Symfony\Component\Form\Form $form
     * @param string[] $options
     * 
     * @return Issue
     */
    public function processValidForm(Form $form, array $options, $entity)
    {
        //canSave?
        $this->provider->canSave($entity);

        // Define creation user
        //$entity->setUser($this->provider->getUser());

        $this->manager->save($entity);

        return $entity;
    }
}

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace OS\IssueBundle\FormManager;

use OS\ToolsBundle\FormManager\AbstractFormModel;
use OS\CommonBundle\Entity\Issues;

/**
 * Description of IssueModel
 *
 * @author ouardisoft
 */
class IssueModel extends AbstractFormModel
{

    public $issueType;
    public $sujet;
    public $nom;
    public $canal;
    public $projet;
    public $bien;
    public $client;
    public $motif;
    public $action;
    public $intervenant;
    public $observation;
    public $responseDate;
    public $status;
    public $delais;
    public $feedback;
    public $isreceive;
    public $customerResponseDate;

    /**
     * 
     * @return Issues
     */
    public function getEntity()
    {
        return parent::getEntity();
    }
}

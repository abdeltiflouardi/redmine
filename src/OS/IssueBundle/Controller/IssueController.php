<?php

namespace OS\IssueBundle\Controller;

use OS\CommonBundle\Controller\BaseController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use OS\CommonBundle\Entity\Issues;

/**
 * @author ouardisoft
 */
class IssueController extends Controller
{
    /**
     * @Route("/", name="_home")
     */
    public function homeAction()
    {
        return $this->renderResponse();
    }

    /**
     * @Route("/issues/", name="_issues")
     */
    public function issuesAction()
    {
        $issueManager = $this->getIssueEntityManager();
        $query = $issueManager->getIssuesQuery(array(), array('i.createdAt' => 'DESC'));

        $this->set('page', $this->pager($query));

        return $this->renderResponse();
    }

    /**
     * @Route("/issue/new/", name="_issue_new")
     */
    public function newAction()
    {
        $form = $this->getIssueFormManager()->createForm();

        $formHandler = $this->getIssueFormManager()->getHandler();
        if ($formHandler->process($form)) {
            $this->flash('Réclamation enregistrée.');

            return $this->redirectTo('_issues');
        }

        $this->set('form', $form->createView());

        return $this->renderResponse();
    }

    /**
     * @Route("/issue/{id}/edit/", name="_issue_edit", requirements={"id" = "\d+"})
     * @ParamConverter("issue", class="OSCommonBundle:Issues")
     */
    public function editAction(Issues $issue)
    {
        $form = $this->getIssueFormManager()->createForm();

        $formHandler = $this->getIssueFormManager()->getHandler();
        if ($formHandler->process($form, array(), $issue)) {
            $this->flash('Réclamation enregistrée.');

            return $this->redirectTo('_issues');
        }

        $this->set('form', $form->createView());

        return $this->renderResponse('OSIssueBundle:Issue:new.html.twig');
    }
}

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OS\IssueBundle\EntityManager;

use OS\ToolsBundle\EntityManager\AbstractEntityManager;
use OS\CommonBundle\Entity\Issues;
use Doctrine\ORM\Query;

/**
 * Description of IssueManager
 *
 * @author ouardisoft
 */
class IssueManager extends AbstractEntityManager
{
    public function getIssuesQuery($where = array(), $orders = array(), $returnCount = false)
    {
        $builder = $this->repository->createQueryBuilder('i');

        foreach ($where as $key => $value) {
            if (preg_match('/^\d+$/', $key)) {
                $builder->andWhere($value);
            } else {
                $builder->andWhere($key);

                // Parse vars
                if (preg_match("/:([a-z]+)/", $key, $output)) {
                    $param = $output[1];
                }

                $builder->setParameter($param, $value);
            }
        }

        foreach ($orders as $field => $order) {
            $builder->orderBy($field, $order);
        }

        $query = $builder->getQuery();

        if ($returnCount) {
            return $query->getSingleScalarResult();
        } else {
            return $query;
        }
    }

    /**
     * 
     * @return Issues[]
     */
    public function getIssues()
    {
        return array();
    }
}

<?php

namespace OS\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Issues
 *
 * @ORM\Table(name="issues")
 * @ORM\Entity
 */
class Issues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="issue_type", type="string", length=64, nullable=true)
     */
    private $issueType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="canal", type="string", length=32, nullable=true)
     */
    private $canal;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=128, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="projet", type="string", length=8, nullable=true)
     */
    private $projet;

    /**
     * @var string
     *
     * @ORM\Column(name="bien", type="string", length=16, nullable=true)
     */
    private $bien;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=128, nullable=true)
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="sujet", type="text", nullable=true)
     */
    private $sujet;

    /**
     * @var string
     *
     * @ORM\Column(name="motif", type="string", length=32, nullable=true)
     */
    private $motif;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="text", nullable=true)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="intervenant", type="string", length=32, nullable=true)
     */
    private $intervenant;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="response_date", type="datetime", nullable=true)
     */
    private $responseDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="customer_response_date", type="datetime", nullable=true)
     */
    private $customerResponseDate;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=32, nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="delais", type="text", nullable=true)
     */
    private $delais;

    /**
     * @var string
     *
     * @ORM\Column(name="feedback", type="text", nullable=true)
     */
    private $feedback;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isreceive", type="boolean", nullable=true)
     */
    private $isreceive;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set issueType
     *
     * @param string $issueType
     * @return Issues
     */
    public function setIssueType($issueType)
    {
        $this->issueType = $issueType;
    
        return $this;
    }

    /**
     * Get issueType
     *
     * @return string 
     */
    public function getIssueType()
    {
        return $this->issueType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Issues
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set canal
     *
     * @param string $canal
     * @return Issues
     */
    public function setCanal($canal)
    {
        $this->canal = $canal;
    
        return $this;
    }

    /**
     * Get canal
     *
     * @return string 
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Issues
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set projet
     *
     * @param string $projet
     * @return Issues
     */
    public function setProjet($projet)
    {
        $this->projet = $projet;
    
        return $this;
    }

    /**
     * Get projet
     *
     * @return string 
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set bien
     *
     * @param string $bien
     * @return Issues
     */
    public function setBien($bien)
    {
        $this->bien = $bien;
    
        return $this;
    }

    /**
     * Get bien
     *
     * @return string 
     */
    public function getBien()
    {
        return $this->bien;
    }

    /**
     * Set client
     *
     * @param string $client
     * @return Issues
     */
    public function setClient($client)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set sujet
     *
     * @param string $sujet
     * @return Issues
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;
    
        return $this;
    }

    /**
     * Get sujet
     *
     * @return string 
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * Set motif
     *
     * @param string $motif
     * @return Issues
     */
    public function setMotif($motif)
    {
        $this->motif = $motif;
    
        return $this;
    }

    /**
     * Get motif
     *
     * @return string 
     */
    public function getMotif()
    {
        return $this->motif;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Issues
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set intervenant
     *
     * @param string $intervenant
     * @return Issues
     */
    public function setIntervenant($intervenant)
    {
        $this->intervenant = $intervenant;
    
        return $this;
    }

    /**
     * Get intervenant
     *
     * @return string 
     */
    public function getIntervenant()
    {
        return $this->intervenant;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Issues
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    
        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set responseDate
     *
     * @param \DateTime $responseDate
     * @return Issues
     */
    public function setResponseDate($responseDate)
    {
        $this->responseDate = $responseDate;
    
        return $this;
    }

    /**
     * Get responseDate
     *
     * @return \DateTime 
     */
    public function getResponseDate()
    {
        return $this->responseDate;
    }

    /**
     * Set customerResponseDate
     *
     * @param \DateTime $customerResponseDate
     * @return Issues
     */
    public function setCustomerResponseDate($customerResponseDate)
    {
        $this->customerResponseDate = $customerResponseDate;
    
        return $this;
    }

    /**
     * Get customerResponseDate
     *
     * @return \DateTime 
     */
    public function getCustomerResponseDate()
    {
        return $this->customerResponseDate;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Issues
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set delais
     *
     * @param string $delais
     * @return Issues
     */
    public function setDelais($delais)
    {
        $this->delais = $delais;
    
        return $this;
    }

    /**
     * Get delais
     *
     * @return string 
     */
    public function getDelais()
    {
        return $this->delais;
    }

    /**
     * Set feedback
     *
     * @param string $feedback
     * @return Issues
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;
    
        return $this;
    }

    /**
     * Get feedback
     *
     * @return string 
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Set isreceive
     *
     * @param boolean $isreceive
     * @return Issues
     */
    public function setIsreceive($isreceive)
    {
        $this->isreceive = $isreceive;
    
        return $this;
    }

    /**
     * Get isreceive
     *
     * @return boolean 
     */
    public function getIsreceive()
    {
        return $this->isreceive;
    }
}
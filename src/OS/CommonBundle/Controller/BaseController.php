<?php

namespace OS\CommonBundle\Controller;

use OS\ToolsBundle\Controller\BaseController as Controller;
use OS\IssueBundle\EntityManager\IssueManager as IssueEntityManager;
use OS\IssueBundle\FormManager\IssueManager as IssueFormManager;

/**
 * Description of BaseController
 *
 * @author ouardisoft
 */
class BaseController extends Controller
{
    /**
     * @return IssueEntityManager
     */
    public function getIssueEntityManager()
    {
        return $this->container->get('os_issue.entity.manager');
    }

    /**
     * @return IssueFormManager
     */
    public function getIssueFormManager()
    {
        return $this->container->get('os_issue.form.manager');
    }
}

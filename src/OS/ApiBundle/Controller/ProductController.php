<?php

namespace OS\ApiBundle\Controller;

use OS\CommonBundle\Controller\BaseController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * @Route("/search/", name="_api_product_search")
     */
    public function searchAction()
    {
        $q = $this->getRequest()->query->get('q');

        $manager = $this->getDoctrine()->getManager('sqlserv');
        $cb = $manager->getRepository('OSApiBundle:Product')->createQueryBuilder('p');
        $cb->where($cb->expr()->like('p.codProd', $cb->expr()->literal($q . '%')));
        $cb->setMaxResults(20);

        $result = $cb->getQuery()->getResult();

        $items = array();
        foreach ($result as $product) {
            $items[$product->getCodProd()] = array(
                'id' => $product->getCodProd(),
                'projet' => $product->getProjet(),
                'ville' => $product->getVille(),
                'attachements' => $product->getAttachements(),
            );
        }

        return $this->renderJson($items);
    }
}

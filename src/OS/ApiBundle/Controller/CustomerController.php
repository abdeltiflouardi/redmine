<?php

namespace OS\ApiBundle\Controller;

use OS\CommonBundle\Controller\BaseController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/customer")
 */
class CustomerController extends Controller
{
    /**
     * @Route("/search/", name="_api_customer_search")
     */
    public function searchAction()
    {
        $q = $this->getRequest()->query->get('q');

        $manager = $this->getDoctrine()->getManager('sqlserv');
        $cb = $manager->getRepository('OSApiBundle:Customer')->createQueryBuilder('c');
        $cb->where($cb->expr()->like('c.cinCl', $cb->expr()->literal($q . '%')));
        $cb->orWhere($cb->expr()->like('c.numCl', $cb->expr()->literal($q . '%')));
        $cb->setMaxResults(20);

        $result = $cb->getQuery()->getResult();

        $items = array();
        foreach ($result as $customer) {
            $items[$customer->getNumCl()] = array(
                'nom' => $customer->getNomCl(),
                'id' => $customer->getNumCl(),
                'prenom' => $customer->getPrenomCl(),
                'cin' => $customer->getCinCl(),
                'tel' => $customer->getTelEmployeur(),
                'adresse' => $customer->getAdresseCl(),
                'email' => $customer->getEmailCl(),
            );
        }

        return $this->renderJson($items);
    }
}

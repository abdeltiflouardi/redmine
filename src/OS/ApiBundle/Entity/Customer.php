<?php

namespace OS\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity
 */
class Customer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="num_cl", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $numCl;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_cl", type="string", length=64, nullable=true)
     */
    private $nomCl;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_cl", type="string", length=64, nullable=true)
     */
    private $prenomCl;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_employeur", type="string", length=32, nullable=true)
     */
    private $telEmployeur;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_cl", type="string", length=128, nullable=true)
     */
    private $adresseCl;

    /**
     * @var string
     *
     * @ORM\Column(name="email_cl", type="string", length=128, nullable=true)
     */
    private $emailCl;

    /**
     * @var string
     *
     * @ORM\Column(name="cin_cl", type="string", length=32, nullable=true)
     */
    private $cinCl;



    /**
     * Get numCl
     *
     * @return integer 
     */
    public function getNumCl()
    {
        return $this->numCl;
    }

    /**
     * Set nomCl
     *
     * @param string $nomCl
     * @return Customer
     */
    public function setNomCl($nomCl)
    {
        $this->nomCl = $nomCl;
    
        return $this;
    }

    /**
     * Get nomCl
     *
     * @return string 
     */
    public function getNomCl()
    {
        return $this->nomCl;
    }

    /**
     * Set prenomCl
     *
     * @param string $prenomCl
     * @return Customer
     */
    public function setPrenomCl($prenomCl)
    {
        $this->prenomCl = $prenomCl;
    
        return $this;
    }

    /**
     * Get prenomCl
     *
     * @return string 
     */
    public function getPrenomCl()
    {
        return $this->prenomCl;
    }

    /**
     * Set telEmployeur
     *
     * @param string $telEmployeur
     * @return Customer
     */
    public function setTelEmployeur($telEmployeur)
    {
        $this->telEmployeur = $telEmployeur;
    
        return $this;
    }

    /**
     * Get telEmployeur
     *
     * @return string 
     */
    public function getTelEmployeur()
    {
        return $this->telEmployeur;
    }

    /**
     * Set adresseCl
     *
     * @param string $adresseCl
     * @return Customer
     */
    public function setAdresseCl($adresseCl)
    {
        $this->adresseCl = $adresseCl;
    
        return $this;
    }

    /**
     * Get adresseCl
     *
     * @return string 
     */
    public function getAdresseCl()
    {
        return $this->adresseCl;
    }

    /**
     * Set emailCl
     *
     * @param string $emailCl
     * @return Customer
     */
    public function setEmailCl($emailCl)
    {
        $this->emailCl = $emailCl;
    
        return $this;
    }

    /**
     * Get emailCl
     *
     * @return string 
     */
    public function getEmailCl()
    {
        return $this->emailCl;
    }

    /**
     * Set cinCl
     *
     * @param string $cinCl
     * @return Customer
     */
    public function setCinCl($cinCl)
    {
        $this->cinCl = $cinCl;
    
        return $this;
    }

    /**
     * Get cinCl
     *
     * @return string 
     */
    public function getCinCl()
    {
        return $this->cinCl;
    }
}
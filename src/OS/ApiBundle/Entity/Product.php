<?php

namespace OS\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cod_prod", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codProd;

    /**
     * @var string
     *
     * @ORM\Column(name="lib_prod", type="string", length=255, nullable=true)
     */
    private $libProd;

    /**
     * @var string
     *
     * @ORM\Column(name="projet", type="string", length=64, nullable=true)
     */
    private $projet;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=64, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="attachements", type="string", length=255, nullable=false)
     */
    private $attachements;



    /**
     * Get codProd
     *
     * @return integer 
     */
    public function getCodProd()
    {
        return $this->codProd;
    }

    /**
     * Set libProd
     *
     * @param string $libProd
     * @return Product
     */
    public function setLibProd($libProd)
    {
        $this->libProd = $libProd;
    
        return $this;
    }

    /**
     * Get libProd
     *
     * @return string 
     */
    public function getLibProd()
    {
        return $this->libProd;
    }

    /**
     * Set projet
     *
     * @param string $projet
     * @return Product
     */
    public function setProjet($projet)
    {
        $this->projet = $projet;
    
        return $this;
    }

    /**
     * Get projet
     *
     * @return string 
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Product
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set attachements
     *
     * @param string $attachements
     * @return Product
     */
    public function setAttachements($attachements)
    {
        $this->attachements = $attachements;
    
        return $this;
    }

    /**
     * Get attachements
     *
     * @return string 
     */
    public function getAttachements()
    {
        return $this->attachements;
    }
}